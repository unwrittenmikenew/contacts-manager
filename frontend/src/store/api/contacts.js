import { post, get, patch } from '@/services/api-client'
export let create = (data) => post('/contacts', data)
export let save = (data, id) => patch(`/contacts/${id}`, data)
export let loadContacts = () => get('/contacts')
export let loadContact = (id) => get(`/contacts/${id}`)