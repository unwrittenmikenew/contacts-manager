
import { post } from '@/services/api-client'

export let login = (data) => post('/users/login', data)
export let logout = () => post('users/logoutAll')