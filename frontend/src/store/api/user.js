import { patch } from '@/services/api-client'
export let save = (data) => patch('/users/me', data)