import Model from './Model'
export default class User extends Model {
    constructor() {
        super()
        this.hydrate({
            userName: '',
            email: '',
            firstName: '',
            lastName: '',
            dateOfBirth: '',
            interests: '',
            country: '',
            contactNumbers: [],
            gender: '',
        })
    }
}