export default class BaseModel {
    hydrate(object) {
        for (let key in object) {
            this[key] = object[key]
        }

        return this
    }
}