import Model from './Model'
export default class RegistrationForm extends Model {
    constructor() {
        super()
        this.hydrate({
            userName: '',
            email: '',
            firstName: '',
            lastName: '',
            dateOfBirth: '',
            password: '',
        })
    }
}