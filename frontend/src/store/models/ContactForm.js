import Model from './Model'
export default class User extends Model {
    constructor() {
        super()
        this.hydrate({
            firstName: '',
            lastName: '',
            email: '',
            notes: '',
            landline: '',
            mobile: '',
        })
    }
}