import Vue from 'vue'
import Vuex from 'vuex'

import authentication from './modules/authentication'
import countries from './modules/countries'
import contact from './modules/contact'
import registration from './modules/registration'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        authentication,
        countries,
        contact,
        registration,
        user,
    }
})