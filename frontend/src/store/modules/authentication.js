import * as api from '@/store/api/auth'
// initial state
const state = {
    token: null,
}

// getters
const getters = {
    isLoggedIn: state => state.token,
    token: state => state.token
}

// actions
const actions = {
    login: ({ commit }, payload) => new Promise((resolve, reject) => {
        return api.login(payload).then(result => {
            commit('setToken', result.data.token)
            return resolve(result)
        }).catch(err => {
            reject(err)
        })
    }),
    logout: ({ commit }) => {
        return api.logout().then(() => {
            commit('setToken', null)
        })
    }
}

// mutations
const mutations = {
    setToken(state, payload) {
        state.token = payload
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}