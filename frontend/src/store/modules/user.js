import * as api from '@/store/api/user'
import User from '@/store/models/User'
// initial state
const state = {
    user: new User(),
}

// getters
const getters = {
    user: state => state.user,
}

// actions
const actions = {
    save: ({ state }) => {

        let updates = {
            firstName: state.user.firstName,
            lastName: state.user.lastName,
            country: state.user.country,
            contactNumbers: state.user.contactNumbers,
            interests: state.user.interests,
            dateOfBirth: state.user.dateOfBirth,
            gender: state.user.gender
        }

        return api.save(updates)
    }
}

// mutations
const mutations = {
    addContact(state) {
        state.user.contactNumbers.push({ type: 'Mobile', number: '' })
    },
    updateContactNumber(state, payload) {
        state.user.contactNumbers[payload.index].number = payload.value
    },
    updateContactType(state, payload) {
        state.user.contactNumbers[payload.index].type = payload.value
    },
    removeContactNumber(state, payload) {
        state.user.contactNumbers.splice(payload.index, 1)
    },
    setUser(state, payload) {
        state.user = payload
    },
    setFirstName(state, payload) {
        state.user.firstName = payload
    },
    setLastName(state, payload) {
        state.user.lastName = payload
    },
    setEmail(state, payload) {
        state.user.email = payload
    },
    setInterests(state, payload) {
        state.user.interests = payload
    },
    setGender(state, payload) {
        state.user.gender = payload
    },
    setDateOfBirth(state, payload) {
        state.user.dateOfBirth = payload
    },
    setCountry(state, payload) {
        state.user.country = payload
    }

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}