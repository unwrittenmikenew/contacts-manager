import * as api from '@/store/api/register'
import RegistrationForm from '@/store/models/RegistrationForm'
// initial state
const state = {
    form: new RegistrationForm(),
    errors: {}
}

// getters
const getters = {
    form: state => state.form,
    errors: state => state.errors
}

// actions
const actions = {
    register: ({ state }) => {
        return api.register(state.form)
    }
}

// mutations
const mutations = {
    setFirstName(state, payload) {
        state.form.firstName = payload
    },
    setLastName(state, payload) {
        state.form.lastName = payload
    },
    setEmail(state, payload) {
        state.form.email = payload
    },
    setPassword(state, payload) {
        state.form.password = payload
    },
    setUserName(state, payload) {
        state.form.userName = payload
    },
    setDateOfBirth(state, payload) {
        state.form.dateOfBirth = payload
    },
    setErrors(state, payload) {
        Object.keys(payload).forEach(key => {
            console.log(payload)
            state.errors[key] = payload[key].message
        })
    },
    clearErrors(state) {
        state.errors = {}
    },
    clearForm(state) {
        state.form = new RegistrationForm()
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}