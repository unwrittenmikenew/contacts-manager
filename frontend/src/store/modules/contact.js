import Vue from 'vue'
import * as api from '@/store/api/contacts'
import ContactForm from '@/store/models/ContactForm'
// initial state

const validations = {
    firstName: (v) => {
        if (!v.length > 0) {
            return 'Please enter a first name'
        } else {
            return false
        }
    },
    lastName: (v) => {
        if (!v.length > 0) {
            return 'Please enter a last name'
        } else {
            return false
        }
    },
    email: (v) => {
        if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(v)) {
            return 'Please enter a valid email address'
        } else {
            return false
        }
    }
}

const state = {
    form: new ContactForm(),
    contacts: [],
    errors: {
    }
}

// getters
const getters = {
    form: state => state.form,
    contacts: state => state.contacts,
    errors: state => state.errors,
}

// actions
const actions = {
    create: ({ state }) => {
        return api.create(state.form).then(result => {
            return result
        }).catch((err) => {
            return err
        })
    },
    save: ({ state }, payload) => {

        let updates = {
            firstName: state.form.firstName,
            lastName: state.form.lastName,
            company: state.form.company,
            mobile: state.form.mobile,
            landline: state.form.landline,
            email: state.form.email,
            notes: state.form.notes
        }

        return api.save(updates, payload)
    },
    loadContacts: ({ commit }) => {
        return api.loadContacts().then(result => {
            commit('setContacts', result.data)
            return result.data
        }).catch((err) => {
            return err
        })
    },
    loadContact: ({ commit }, payload) => {
        return api.loadContact(payload).then(result => {
            commit('setContact', result.data)
            return result.data
        }).catch((err) => {
            return err
        })
    },
    validate: ({ state, commit }) => {
        let validatorsTriggered = []
        Object.keys(state.form).forEach(item => {
            let value = state.form[item]
            let validator = validations[item]
            if (validator) {
                let validationTriggered = validator(value)
                if (validationTriggered) {
                    validatorsTriggered.push({ key: item, message: validationTriggered })
                    commit('setError', { key: item, message: validationTriggered })
                }
            }
        })
        return validatorsTriggered
    }
}

// mutations
const mutations = {
    setFirstName(state, payload) {
        state.form.firstName = payload
    },
    setLastName(state, payload) {
        state.form.lastName = payload
    },
    setCompany(state, payload) {
        state.form.company = payload
    },
    setEmail(state, payload) {
        state.form.email = payload
    },
    setLandline(state, payload) {
        state.form.landline = payload
    },
    setMobile(state, payload) {
        state.form.mobile = payload
    },
    setNotes(state, payload) {
        state.form.notes = payload
    },
    setErrors(state, payload) {
        state.errors = payload
    },
    setError(state, payload) {
        Vue.set(state.errors, payload.key, payload.message)
    },
    clearErrors(state) {
        state.errors = {}
    },
    clearForm(state) {
        state.form = new ContactForm()
    },
    createContact(state, payload) {
        state.contacts.push(payload)
    },
    setContacts(state, payload) {
        state.contacts = payload
    },
    setContact(state, payload) {
        state.form = payload
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}