import * as api from '@/store/api/countries'
const state = {
    countries: [],
}

const getters = {
    countries: state => state.countries,
}
const actions = {
    loadCountries: ({ commit }, payload) => new Promise((resolve, reject) => {
        return api.loadCountries(payload).then(result => {
            let mappedCountries = result.data.map((country) => {
                return {
                    text: country.name, value: country.name
                }
            })
            mappedCountries.unshift({ text: 'Please select', value: '' })
            commit('setCountries', mappedCountries)
            return resolve(result)
        }).catch(err => {
            reject(err)
        })
    }),
}

const mutations = {
    setCountries(state, payload) {
        state.countries = payload
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}