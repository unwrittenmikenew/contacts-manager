import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false
import Page from '@/components/app/Page'

Vue.component('page', Page)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
