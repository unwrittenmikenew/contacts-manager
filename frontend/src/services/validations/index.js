const nameValidation = (v) => !!v || 'Name is required'
const passwordValidation = (v) => !!v || 'Password is required'

export {
    nameValidation,
    passwordValidation
}
