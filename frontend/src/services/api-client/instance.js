import axios from 'axios'
import store from '@/store'
import router from '@/router'
let instance = axios.create({
  baseURL: process.env.VUE_APP_API_ROOT,
});

instance.interceptors.request.use(function (config) {
  const token = store.getters['authentication/token']
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }
  return config;
}, function (err) {
  return Promise.reject(err);
});

instance.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  if (401 === error.response.status) {
    router.push({ path: "/login" })
  } else {
    return Promise.reject(error);
  }
});


export default instance