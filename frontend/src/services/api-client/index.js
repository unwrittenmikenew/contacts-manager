import http from './instance'

export let get = (url, success, failure, options = {}) => {
  return http.get(url, options).then(success).catch(failure)
}

export let post = (url, data) => {
  return http.post(url, data)
}

export let patch = (url, data) => {
  return http.patch(url, data)
}
