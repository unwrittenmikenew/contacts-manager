import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

Vue.use(VueRouter)

import Home from '@/views/Home'
import Login from '@/views/Login'
import Register from '@/views/Register'
import Contacts from '@/views/Contacts'
import Profile from '@/views/Profile'
import CreateContact from '@/views/CreateContact'
import ViewContact from '@/views/ViewContact'
import Palindrome from '@/views/Palindrome'
import Anagram from '@/views/Anagram'
import ErrorPage from '@/views/ErrorPage'
const routes = [
    { name: 'home', path: '/', component: Home },
    { name: 'login', path: '/login', component: Login },
    { name: 'register', path: '/register', component: Register },
    {
        name: 'profile',
        path: '/profile',
        component: Profile,
        meta: {
            requiresAuth: true
        }
    },
    {
        name: 'contacts',
        path: '/contacts',
        component: Contacts,
        meta: {
            requiresAuth: true
        }
    },
    {
        name: 'create-contact',
        path: '/contacts/create',
        component: CreateContact,
        meta: {
            requiresAuth: true
        }
    },
    {
        name: 'view-contact',
        path: '/contacts/:id',
        component: ViewContact,
        meta: {
            requiresAuth: true
        }
    },
    {
        name: 'palindrome',
        path: '/palindrome',
        component: Palindrome
    },
    {
        name: 'anagram',
        path: '/anagram',
        component: Anagram
    },
    {
        name: 'error',
        path: '*',
        component: ErrorPage,
    },
]

const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters['authentication/isLoggedIn']) {
            next()
            return
        }
        next('/login')
    } else {
        next()
    }
})

export default router