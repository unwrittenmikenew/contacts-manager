const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const userSchema = new mongoose.Schema({
    userName: {
        type: String,
        required: true,
        trim: true,
        minLength: 2,
        unique: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        lowercase: true,
        validate: (value) => {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid')
            }
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 7,
        validate: (value) => {
            if (value.toLowerCase().includes('password')) {
                throw new Error('Password is invalid')
            }
        }
    },
    firstName: {
        type: String,
        required: true,
        trim: true
    },
    lastName: {
        type: String,
        required: true,
        trim: true
    },
    dateOfBirth: {
        type: String,
        required: true,
        trim: true
    },
    interests: {
        type: String,
        required: false,
        trim: true,
        default: '',
    },
    country: {
        type: String,
        required: false,
        trim: true,
        default: '',
    },
    contactNumbers: [],
    gender: {
        type: String,
        required: false,
        trim: true,
        default: '',
    },
    dateOfBirth: {
        type: String,
        required: true,
        trim: true,
        default: '',
    },
    tokens: [
        {
            token: {
                type: String,
                required: true
            }
        }
    ]
})
//Find user by email or username
userSchema.statics.findByCredentials = async (emailOrUsername, password) => {
    let user;
    const isEmail = validator.isEmail(emailOrUsername)

    if (isEmail) {
        user = await User.findOne({ email: emailOrUsername })
    } else {
        user = await User.findOne({ userName: emailOrUsername })
    }
    //Cannot find a user with the specified email or username
    if (!user) {
        throw new Error('Credentials incorrect')
    }

    const isMatch = await bcrypt.compare(password, user.password)
    //Password incorrect
    if (!isMatch) {
        throw new Error('Credentials incorrect')
    }

    return user
}
//Generate a JSON Web Token for the user
userSchema.methods.generateAuthToken = async function () {
    const user = this
    const token = jwt.sign({ _id: user.id.toString() }, 'mysecretkey')
    user.tokens = user.tokens.concat({ token })
    await user.save()
    return token
}
//Return the user as JSON but do not expose sensitive info like the password
userSchema.methods.toJSON = function () {
    const user = this
    const userObject = user.toObject()
    delete userObject.password
    delete userObject.tokens
    return userObject
}
//Hash the plain text password before we save the user model
userSchema.pre('save', async function (next) {
    const user = this

    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }

    next()
})

const User = mongoose.model('User', userSchema)

module.exports = User
