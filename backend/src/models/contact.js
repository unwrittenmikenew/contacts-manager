const mongoose = require('mongoose')
const validator = require('validator')


const contactSchema = new mongoose.Schema({

    firstName: {
        type: String,
        required: true,
        trim: true
    },
    lastName: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true
    },
    notes: {
        type: String,
        default: ''
    },
    company: {
        type: String,
        default: ''
    },
    landline: {
        type: String,
        default: ''
    },
    mobile: {
        type: String,
        default: ''
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    }

})

const Contact = mongoose.model('Contact', contactSchema)

module.exports = Contact
