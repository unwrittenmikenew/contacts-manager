const express = require('express')
const dotenv = require('dotenv');
const cors = require('cors')
//Expose enviroment variables
dotenv.config();
//Init the database connection
require('./db/mongoose')

const userRouter = require('./routers/user')
const contactsRouter = require('./routers/contacts')
const app = express()
const port = process.env.PORT || 3000
app.use(cors())
app.use(express.json())
app.use(userRouter)
app.use(contactsRouter)
//Start server
app.listen(port, () => {
    console.log('Server is running on ' + port)
})