const jwt = require('jsonwebtoken')
const User = require('../models/user')

const auth = async (req, res, next) => {
    try {
        //Extract token from request
        const token = req.header('Authorization').replace('Bearer ', '')
        //Decode the token
        const decoded = jwt.verify(token, 'mysecretkey')
        //Find the user in the database
        const user = await User.findOne({ _id: decoded._id, 'tokens.token': token })
        if (!user) {
            throw new Error()
        }
        //Add token to request
        req.token = token
        //Add user to request
        req.user = user
        next()
    } catch (e) {
        //If no user was found send a 401 response
        res.status(401).send({ error: 'Please authenticate' })
    }
}

module.exports = auth