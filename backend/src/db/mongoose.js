const mongoose = require('mongoose')

//Connect to mongodb database
mongoose.connect(`mongodb://${process.env.DBUSER}:${process.env.DBPASS}@ds263156.mlab.com:63156/contact-manager`, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
})

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('Database connected!')
});