const express = require('express')
const Contact = require('../models/contact')
const auth = require('../middleware/auth')
const router = new express.Router()
//Create a contact
router.post('/contacts', auth, async (req, res) => {
    const contact = new Contact({ ...req.body, owner: req.user._id })
    try {
        await contact.save()
        res.status(201).send(contact)
    } catch (e) {
        console.log(e)
        res.status(400).send(e)
    }
})
//Get all contacts for logged in user
router.get('/contacts', auth, async (req, res) => {
    try {
        const contacts = await Contact.find({ owner: req.user._id })
        res.status(201).send(contacts)
    } catch (e) {
        res.status(500).send('Server error')
    }
})
//Get a contact by id for logged in user
router.get('/contacts/:id', auth, async (req, res) => {
    const _id = req.params.id
    try {
        const contact = await Contact.findOne({ _id, owner: req.user._id })
        if (!contact) {
            return res.status(404).send()
        }
        res.send(contact)
    } catch (e) {
        res.status(500).send('Server error')
    }
})
//Update a contact by id for logged in user
router.patch('/contacts/:id', auth, async (req, res) => {
    const _id = req.params.id
    const updates = Object.keys(req.body)
    const allowedUpdates = ["firstName", "lastName", "email", "notes", "company", "mobile", "landline"]
    const isValidOperation = updates.every((update) => {
        return allowedUpdates.includes(update)
    })
    if (!isValidOperation) {
        res.status(400).send({ error: 'Invalid updates!' })
    }
    try {
        const contact = await Contact.findOne({ _id: _id, owner: req.user._id })
        if (!contact) {
            return res.status(404).send()
        }
        updates.forEach((update) => contact[update] = req.body[update])
        await contact.save()
        res.send(contact)
    } catch (e) {
        res.status(400).send(e)
    }
})

//Delete a contact for logged in user
router.delete('/contacts/:id', auth, async (req, res) => {
    try {
        const contact = await Contact.findOneAndDelete({ _id: req.params.id, owner: req.user._id })
        if (!contact) {
            return res.status(404).send()
        }
        res.send(contact)
    } catch (e) {
        res.status(400).send()
    }
})

module.exports = router